package models

import "github.com/jinzhu/gorm"

type Customer struct {
	gorm.Model
	InvoiceID   uint `gorm:"not null"`
	Name    string
	TaxNo   string
	Email   string
	Phone   string
	Address string
}

type CustomerService interface {
	Create(customer *Customer) error
	GetByInvoiceID(id uint) (*Customer, error)
}

func NewCustomerService(db *gorm.DB) CustomerService {
	return &customerGorm{
		db: db,
	}
}

type customerGorm struct {
	db *gorm.DB
}

func (cg *customerGorm) Create(customer *Customer) error {
	return cg.db.Create(customer).Error
}

func (cg *customerGorm) GetByInvoiceID(id uint) (*Customer, error) {
	customer := new(Customer)
	err := cg.db.First(customer, "invoice_id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return customer, nil
}
package models

import (
	"github.com/jinzhu/gorm"
)

type Logo struct {
	gorm.Model
	CompanyID uint   `gorm:"not null"`
	Filename  string `gorm:"not null"`
}

type LogoService interface {
	Create(logo *Logo) error
	GetByCompanyID(id uint) (*Logo, error)
}

func NewLogoService(db *gorm.DB) LogoService {
	return &logoGorm{
		db: db,
	}
}

type logoGorm struct {
	db *gorm.DB
}

func (lg *logoGorm) Create(logo *Logo) error {
	return lg.db.Create(logo).Error
}

func (lg *logoGorm) GetByCompanyID(id uint) (*Logo, error) {
	logo := new(Logo)
	err := lg.db.First(logo, "company_id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return logo, nil
}
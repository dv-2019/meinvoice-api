package models

import (
	"github.com/jinzhu/gorm"
)

type Company struct {
	gorm.Model
	InvoiceID uint `gorm:"not null"`
	Name      string
	TaxNo     string
	Email     string
	Phone     string
	Address   string
}

type CompanyService interface {
	Create(company *Company) error
	GetByInvoiceID(id uint) (*Company, error)
}

func NewCompanyService(db *gorm.DB) CompanyService {
	return &companyGorm{
		db: db,
	}
}

type companyGorm struct {
	db *gorm.DB
}

func (cg *companyGorm) Create(company *Company) error {
	return cg.db.Create(company).Error
}

func (cg *companyGorm) GetByInvoiceID(id uint) (*Company, error) {
	company := new(Company)
	err := cg.db.First(company, "invoice_id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return company, nil
}

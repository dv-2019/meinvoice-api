package models

import (
	"github.com/jinzhu/gorm"
)

type Invoice struct {
	gorm.Model
	No        string
	IssueDate string
	DueDate   string
	Note      string
}

type InvoiceService interface {
	Create(invoice *Invoice) error
	List() ([]Invoice, error)
	GetByID(id uint) (*Invoice, error)
}

func NewInvoiceService(db *gorm.DB) InvoiceService {
	return &invoiceGorm{
		db: db,
	}
}

type invoiceGorm struct {
	db *gorm.DB
}

func (ig *invoiceGorm) Create(invoice *Invoice) error {
	return ig.db.Create(invoice).Error
}

func (ig *invoiceGorm) List() ([]Invoice, error) {
	invoices := []Invoice{}
	if err := ig.db.Find(&invoices).Error; err != nil {
		return nil, err
	}
	return invoices, nil
}

func (ig *invoiceGorm) GetByID(id uint) (*Invoice, error) {
	invoice := new(Invoice)
	if err := ig.db.First(invoice, id).Error; err != nil {
		return nil, err
	}
	return invoice, nil
}
package models

import (
	"github.com/jinzhu/gorm"
)

type Item struct {
	gorm.Model
	InvoiceID   uint `gorm:"not null"`
	Description string
	Qty         string
	UnitPrice   string
	TotalPrice  string
}

type ItemService interface {
	Create(item *Item) error
	GetByInvoiceID(id uint) ([]Item, error)
}

func NewItemService(db *gorm.DB) ItemService {
	return &itemGorm{
		db: db,
	}
}

type itemGorm struct {
	db *gorm.DB
}

func (ig *itemGorm) Create(item *Item) error {
	return ig.db.Create(item).Error
}

func (ig *itemGorm) GetByInvoiceID(id uint) ([]Item, error) {
	items := []Item{}
	err := ig.db.
		Order("created_at DESC").
		Where("invoice_id = ?", id).
		Find(&items).Error
	if err != nil {
		return nil, err
	}
	return items, nil
}

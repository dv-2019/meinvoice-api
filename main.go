package main

import (
	"log"
	"meinvoice-api/handlers"
	"meinvoice-api/models"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	db, err := gorm.Open(
		"mysql",
		"root:password@tcp(poomdv.c52jeww5mzql.ap-southeast-1.rds.amazonaws.com:3306)/meinvoice?parseTime=true",
	)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true) // dev only!

	// err = models.Reset(db)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	cos := models.NewCompanyService(db)
	cus := models.NewCustomerService(db)
	its := models.NewItemService(db)
	ins := models.NewInvoiceService(db)
	ls := models.NewLogoService(db)

	h := handlers.NewHandler(cos, cus, its, ins, ls)

	r := gin.Default()

	r.POST("/invoices", h.Create)

	r.GET("/invoices", h.ListInvoice)

	r.GET("/invoices/:id", h.GetById)

	r.Run(":8080")
}

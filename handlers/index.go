package handlers

import (
	"meinvoice-api/models"
)

type Handler struct {
	cos models.CompanyService
	cus models.CustomerService
	its models.ItemService
	ins models.InvoiceService
	ls  models.LogoService
}

func NewHandler(cos models.CompanyService, cus models.CustomerService,
	its models.ItemService, ins models.InvoiceService, ls models.LogoService) *Handler {
	return &Handler{cos, cus, its, ins, ls}
}

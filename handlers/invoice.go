package handlers

import (
	"fmt"
	"meinvoice-api/models"
	"strconv"

	"github.com/gin-gonic/gin"
)

type InvoiceRes struct {
	ID        uint     `json:"id"`
	No        string   `json:"no"`
	Company   Company  `json:"company"`
	Customer  Customer `json:"customer"`
	IssueDate string   `json:"issueDate"`
	DueDate   string   `json:"dueDate"`
	Items     []Item   `json:"items"`
	Note      string   `json:"note"`
}

type CreateReq struct {
	InvoiceRes
}

func (h *Handler) Create(c *gin.Context) {
	req := new(CreateReq)
	if err := c.BindJSON(req); err != nil {
		Error(c, 400, err)
		return
	}
	// fmt.Println(req)

	invoice := new(models.Invoice)
	invoice.No = req.No
	invoice.IssueDate = req.IssueDate
	invoice.DueDate = req.DueDate
	invoice.Note = req.Note
	if err := h.ins.Create(invoice); err != nil {
		Error(c, 500, err)
		return
	}

	company := new(models.Company)
	company.Name = req.Company.Name
	company.TaxNo = req.Company.TaxNo
	company.Email = req.Company.Email
	company.Phone = req.Company.Phone
	company.Address = req.Company.Address
	company.InvoiceID = invoice.ID

	if err := h.cos.Create(company); err != nil {
		Error(c, 500, err)
		return
	}

	logo := new(models.Logo)
	logo.CompanyID = company.ID
	logo.Filename = req.Company.Logo

	if err := h.ls.Create(logo); err != nil {
		Error(c, 500, err)
		return
	}

	customer := new(models.Customer)
	customer.Name = req.Customer.Name
	customer.TaxNo = req.Customer.TaxNo
	customer.Email = req.Customer.Email
	customer.Phone = req.Customer.Phone
	customer.Address = req.Customer.Address
	customer.InvoiceID = invoice.ID

	if err := h.cus.Create(customer); err != nil {
		Error(c, 500, err)
		return
	}

	item := Item{}
	for _, item = range req.Items {
		itemModel := new(models.Item)
		itemModel.Description = item.Description
		itemModel.Qty = item.Qty
		itemModel.UnitPrice = item.UnitPrice
		itemModel.TotalPrice = item.TotalPrice
		itemModel.InvoiceID = invoice.ID

		if err := h.its.Create(itemModel); err != nil {
			Error(c, 500, err)
			return
		}
	}

	c.JSON(201, invoice)
}

func (h *Handler) ListInvoice(c *gin.Context) {
	data, err := h.ins.List()
	if err != nil {
		Error(c, 500, err)
		return
	}
	invoices := []InvoiceRes{}
	for _, d := range data {
		fmt.Println(d)
		companyModel, err := h.cos.GetByInvoiceID(d.ID)
		if err != nil {
			Error(c, 500, err)
			return
		}
		logoModel, err := h.ls.GetByCompanyID(companyModel.ID)
		if err != nil {
			Error(c, 500, err)
			return
		}
		company := Company{}
		company.Name = companyModel.Name
		company.TaxNo = companyModel.TaxNo
		company.Email = companyModel.Email
		company.Phone = companyModel.Phone
		company.Address = companyModel.Address
		company.Logo = logoModel.Filename

		customerModel, err := h.cus.GetByInvoiceID(d.ID)
		if err != nil {
			Error(c, 500, err)
			return
		}
		customer := Customer{}
		customer.Name = customerModel.Name
		customer.TaxNo = customerModel.TaxNo
		customer.Email = customerModel.Email
		customer.Phone = customerModel.Phone
		customer.Address = customerModel.Address

		itemsModel, err := h.its.GetByInvoiceID(d.ID)
		if err != nil {
			Error(c, 500, err)
			return
		}
		items := []Item{}
		for _, i := range itemsModel {
			items = append(items, Item{
				Description: i.Description,
				Qty:         i.Qty,
				UnitPrice:   i.UnitPrice,
				TotalPrice:  i.TotalPrice,
			})
		}

		invoices = append(invoices, InvoiceRes{
			ID:        d.ID,
			No:        d.No,
			Company:   company,
			Customer:  customer,
			IssueDate: d.IssueDate,
			DueDate:   d.DueDate,
			Items:     items,
			Note:      d.Note,
		})
	}
	c.JSON(200, invoices)
}

func (h *Handler) GetById(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		Error(c, 400, err)
		return
	}
	data, err := h.ins.GetByID(uint(id))
	if err != nil {
		Error(c, 500, err)
		return
	}
	fmt.Println(data)

	companyModel, err := h.cos.GetByInvoiceID(data.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	logoModel, err := h.ls.GetByCompanyID(companyModel.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	company := Company{}
	company.Name = companyModel.Name
	company.TaxNo = companyModel.TaxNo
	company.Email = companyModel.Email
	company.Phone = companyModel.Phone
	company.Address = companyModel.Address
	company.Logo = logoModel.Filename

	customerModel, err := h.cus.GetByInvoiceID(data.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	customer := Customer{}
	customer.Name = customerModel.Name
	customer.TaxNo = customerModel.TaxNo
	customer.Email = customerModel.Email
	customer.Phone = customerModel.Phone
	customer.Address = customerModel.Address

	itemsModel, err := h.its.GetByInvoiceID(data.ID)
	if err != nil {
		Error(c, 500, err)
		return
	}
	items := []Item{}
	for _, i := range itemsModel {
		items = append(items, Item{
			Description: i.Description,
			Qty:         i.Qty,
			UnitPrice:   i.UnitPrice,
			TotalPrice:  i.TotalPrice,
		})
	}
	invoice:= InvoiceRes{
		ID:        data.ID,
		No:        data.No,
		Company:   company,
		Customer:  customer,
		IssueDate: data.IssueDate,
		DueDate:   data.DueDate,
		Items:     items,
		Note:      data.Note,
	}

	c.JSON(200,invoice)
}

package handlers

type Company struct {
	Name    string `json:"name"`
	TaxNo   string `json:"taxId"`
	Email   string `json:"email"`
	Phone   string `json:"phone"`
	Address string `json:"address"`
	Logo    string `json:"logo"`
}

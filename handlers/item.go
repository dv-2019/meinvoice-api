package handlers

type Item struct {
	Description string `json:"description"`
	Qty         string `json:"qty"`
	UnitPrice   string `json:"unitPrice"`
	TotalPrice  string `json:"totalPrice"`
}
